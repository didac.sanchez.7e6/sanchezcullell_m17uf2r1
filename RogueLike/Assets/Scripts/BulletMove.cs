using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Movimiento))]
public class BulletMove : MonoBehaviour 
{
    private Movimiento move;
    public bool isEnemy;
    public GameObject spawnPoint;
    public string spawnPointName;
    public string playerName;
    public float posX, posY;
    public GameObject player;
    public int damage;
    // Start is called before the first frame update
    void OnEnable()
    {
        if(!isEnemy) damage = SWeaponeSniper.instance.data.damage;
        else damage = Shot.instance.data.damage;
        player = GameObject.Find(playerName);
        spawnPoint = GameObject.Find(spawnPointName);
        move = GetComponent<Movimiento>();
        posX = spawnPoint.transform.position.x - player.transform.position.x;
        posY = spawnPoint.transform.position.y - player.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(new Vector2(posX, posY).normalized);
        move.MoveThing(new Vector2(posX, posY).normalized);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        this.gameObject.SetActive(false);
    }
}
