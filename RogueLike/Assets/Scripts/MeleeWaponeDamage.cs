using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class MeleeWaponeDamage : Weapon
{
    private Collider2D _col;
    // Start is called before the first frame update
    void Start()
    {
        _col = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    public override void Attack()
    {
        _col.enabled = true;
    }
    public override void NoAttack()
    {
        _col.enabled = false;
    }
}
