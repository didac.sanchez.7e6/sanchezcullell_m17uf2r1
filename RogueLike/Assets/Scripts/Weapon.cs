using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    public int damage;
    public int cost;
    public float secons;
    // public typedamage typedamage;
    // Start is called before the first frame update
    private void OnEnable()
    {
        GameManager.OnAttack += Attack;
        GameManager.OnNoAttack += NoAttack;
    }
    public abstract void Attack();
    public abstract void NoAttack();


}
