﻿using UnityEngine;
[CreateAssetMenu(fileName = "New Health", menuName = "SHealth")]
public class SOHealth : ScriptableObject
{
    public int healthMax;
}
