using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SWeaponMelee : ReadSWeapone
{
    [SerializeField] private Collider2D _col;
    // Start is called before the first frame update
    public void SetUp()
    {
        _col = GameObject.Find("MeleeDamage").GetComponent<Collider2D>();
    }

    // Update is called once per frame
    public void Attack()
    {
        Debug.Log(_col.isActiveAndEnabled);
        _col.enabled = true;
        Debug.Log(_col.isActiveAndEnabled);
    }
    public void NoAttack()
    {
        _col.enabled = false;
    }
}
