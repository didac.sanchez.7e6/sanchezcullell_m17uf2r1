using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class Inventori : MonoBehaviour
{
    public List<GameObject> weapones;
    public GameObject weapon;
    private bool melee = false;
    private bool sniper = false;
    private bool flamethrower = false;

    private void Awake()
    {
        GameManager.OnAttack += Attack;
        GameManager.OnNoAttack += NoAttack;
        weapon = weapones[0];
        weapon.GetComponent<SWeaponMelee>().SetUp();
        melee = true;
    }
    public void Attack()
    {
        if (melee) weapon.GetComponent<SWeaponMelee>().Attack();
        if(sniper) weapon.GetComponent<SWeaponeSniper>().Attack();
        if (flamethrower){ }
    }
    public void NoAttack()
    {
        if (melee) weapon.GetComponent<SWeaponMelee>().NoAttack();
    }

    private void OnGUI()
    {
        //Se que esta mal, no tengo tiempo para mas
        if (Event.current.Equals(Event.KeyboardEvent(KeyCode.Keypad1.ToString())))
        {
            weapon = weapones[0];
            melee = true;
            sniper = false;
            flamethrower = false;
            weapon.GetComponent<SWeaponMelee>().SetUp();
        }

        if (Event.current.Equals(Event.KeyboardEvent(KeyCode.Keypad2.ToString())))
        {
            weapon = weapones[1];
            melee = false;
            sniper = true;
            flamethrower = false;
            weapon.GetComponent<SWeaponeSniper>().SetUp();
        }

        if (Event.current.Equals(Event.KeyboardEvent(KeyCode.Keypad3.ToString())))
        {
            weapon = weapones[2];
            melee = false;
            sniper = false;
            flamethrower = true;
        }

    }
}
