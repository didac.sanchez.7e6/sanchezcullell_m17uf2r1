using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "SWeapon")]
public class WeaponSO : ScriptableObject
{
    public int damage;
    public int cost;
}
