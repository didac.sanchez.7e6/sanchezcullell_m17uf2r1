using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SWeaponeSniper : ReadSWeapone
{

    public static SWeaponeSniper instance;
    public GameObject bullet = null;
    public GameObject rotatinRefernse;
    public GameObject spawnPoint;
    // Start is called before the first frame update
    public void SetUp()
    {
        instance = this;
        spawnPoint = GameObject.Find("MeleeDamage");
        rotatinRefernse = GameObject.Find("MeleeRotationPoint");
    }


    public void Attack()
    {
        bullet = PoolManager.instance.RequestBulet();
        if (bullet != null)
        {
            bullet.transform.position = spawnPoint.transform.position; 
            bullet.transform.rotation = rotatinRefernse.transform.rotation;
            bullet.SetActive(true);
        }
    }


}
