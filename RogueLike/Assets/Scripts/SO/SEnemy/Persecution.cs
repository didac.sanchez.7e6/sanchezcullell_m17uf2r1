using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Persecution : MonoBehaviour
{
    private Movimiento move;
    public Transform Player;
    public float posX, posY;
    public SOEnemy data;
    void Awake()
    {
        move = GetComponent<Movimiento>();
    }
    void Update()
    {
        if (data.detectionRadius > Vector3.Distance(transform.position, Player.position))
        {
            Haunt();
        }
    }
    private void Haunt()
    {
        posX = Player.position.x - transform.position.x;
        posY = Player.position.y - transform.position.y;
        move.MoveThing(new Vector2(posX, posY).normalized);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        collision.gameObject.GetComponent<Health>().Hurt(data.damage);
        this.GetComponent<Health>().Dead();
    }
}

