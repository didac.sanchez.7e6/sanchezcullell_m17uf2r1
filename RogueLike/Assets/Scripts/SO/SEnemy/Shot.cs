using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour
{
    public GameObject bullet = null;
    public Transform Player;
    public SOEnemy data;
    void Update()
    {
        if (data.detectionRadius > Vector3.Distance(transform.position, Player.position))
        {
            ShotThing();
        }
    }

    private void ShotThing()
    {
        bullet = RequestBulet();
        bullet.transform.position = transform.position;
        bullet.SetActive(true);
    }
    public GameObject prefap;
    public List<GameObject> pool;
    public static Shot instance;
    void Awake()
    {
        instance = this;
        GameObject poolBullet;
        for (int i = 0; i < 20; i++)
        {
            poolBullet = Instantiate(prefap);
            pool.Add(poolBullet);
            poolBullet.SetActive(false);
            poolBullet.transform.parent = transform;
        }

    }

    private void AddToPool()
    {
        GameObject poolBullet = Instantiate(prefap);
        pool.Add(poolBullet);
        poolBullet.SetActive(true);
        poolBullet.transform.parent = transform;
    }
    public GameObject RequestBulet()
    {
        foreach (GameObject letBullet in pool)
        {
            if (!letBullet.activeSelf)
            {
                letBullet.SetActive(true);
                return letBullet;
            }
        }
        AddToPool();
        return pool[pool.Count - 1];
    }
}
