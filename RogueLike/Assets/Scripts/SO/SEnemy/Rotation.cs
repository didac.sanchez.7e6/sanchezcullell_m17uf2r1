using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    public Transform Player;
    private Movimiento move;
    // Update is called once per frame
    void Start()
    {
        move = GetComponent<Movimiento>();
    }
    void Update()
    {
        move.RotateThing(AngleCalculator());
    }
    private float AngleCalculator()
    {
        
        
        return Mathf.Atan2(transform.position.y - Player.position.y, transform.position.x - Player.position.x) * Mathf.Rad2Deg;
    }
}
