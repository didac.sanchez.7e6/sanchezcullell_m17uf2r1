using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName = "SEnemy")]
public class SOEnemy : ScriptableObject
{
    public float detectionRadius = 25f;
    public int damage;
}
