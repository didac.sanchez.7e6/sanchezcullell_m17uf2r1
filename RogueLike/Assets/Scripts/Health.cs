using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour, IDamages
{
    public int actualHealth;
    public SOHealth data;
    // Start is called before the first frame update
    void Awake()
    {
        actualHealth = data.healthMax;
    }
    public void Hurt(int damage)
    {
        Debug.Log("Pain");
        actualHealth = Mathf.Max(actualHealth - damage, 0);
        if (actualHealth == 0)
        {
            Dead();
        }
    }
    public void Dead()
    {
        Debug.Log("Muere");
        Destroy(gameObject);
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Hi");
        if(collision.gameObject.GetComponent<Weapon>() != null) Hurt(collision.gameObject.GetComponent<Weapon>().damage);
        else Hurt(collision.gameObject.GetComponent<BulletMove>().damage);
    }
}
