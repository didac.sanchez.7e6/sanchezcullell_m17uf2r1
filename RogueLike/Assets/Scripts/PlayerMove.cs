using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

[RequireComponent(typeof(Movimiento))]
public class PlayerMove : MonoBehaviour
{
    private Animator anim;
    private Movimiento move;
  
    void Awake()
    {
        move = GetComponent<Movimiento>();
        anim = GetComponent<Animator>();
        
    }
    private void OnEnable()
    {
        GameManager.OnMove += Move;

    }
    private void Move(Vector2 direction)
    {       
        move.MoveThing(new Vector2(direction.x, direction.y).normalized);
        anim.SetFloat("PosX", direction.x);
        anim.SetFloat("PosY", direction.y);
    }
}
