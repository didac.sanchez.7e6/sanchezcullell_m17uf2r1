using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;
[RequireComponent(typeof(Movimiento))]

public class MeleeWeaponMove : MonoBehaviour
{

    private Movimiento move;
    // Update is called once per frame
    void Start()
    {
        move = GetComponent<Movimiento>();
    }
    void Update()
    {
        move.RotateThing(AngleCalculator() - 90);
    }
    private float AngleCalculator()
    {
        Vector3 mousePos =  Camera.main.ScreenToViewportPoint(Input.mousePosition);
        Vector2 posOnScreen = Camera.main.WorldToViewportPoint(transform.position);
        return Mathf.Atan2(posOnScreen.y - mousePos.y, posOnScreen.x - mousePos.x) * Mathf.Rad2Deg;
    }
}
