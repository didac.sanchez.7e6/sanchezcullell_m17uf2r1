using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    private Rigidbody2D _rb;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    public void MoveThing(Vector3 direction)
    {
        _rb.velocity = direction * speed;
    }
    public void RotateThing(float pos)
    {
        transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, pos));
    }
}
