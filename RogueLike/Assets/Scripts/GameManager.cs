using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.InputSystem;

public class GameManager : MonoBehaviour
{
    private PlayerInput playerInput;
    public static event Action<Vector2> OnMove = delegate { };
    public static event Action OnAttack = delegate { };
    public static event Action OnNoAttack = delegate { };
    void Awake()
    {
        playerInput = GetComponent<PlayerInput>();
    }

    private void OnEnable()
    {
        playerInput.actions["Move"].performed += Move;
        playerInput.actions["Move"].canceled += Move;
        playerInput.actions["Attack"].performed += Attack;
        playerInput.actions["Attack"].canceled += NoAttack;
    }
    private void Move(InputAction.CallbackContext ctx)
    {
        OnMove.Invoke(ctx.ReadValue<Vector2>());
    }
    private void Attack(InputAction.CallbackContext ctx)
    {
        OnAttack.Invoke();
    }
    private void NoAttack(InputAction.CallbackContext ctx)
    {
        OnNoAttack.Invoke();
    }

}
