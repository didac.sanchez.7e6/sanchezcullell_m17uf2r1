using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    public GameObject prefap;
    public List<GameObject> pool;
    public static PoolManager instance;
    // Start is called before the first frame update
    void Awake()
    {
        GameObject poolBullet;
        for (int i = 0; i < 20; i++)
        {
            poolBullet = Instantiate(prefap);
            pool.Add(poolBullet);
            poolBullet.SetActive(false);
            poolBullet.transform.parent = transform;
        }
        instance = this;
    }

    private void AddToPool()
    {
        GameObject poolBullet = Instantiate(prefap);
        pool.Add(poolBullet);
        poolBullet.SetActive(true);
        poolBullet.transform.parent = transform;
    }
    public GameObject RequestBulet()
    {
        foreach (GameObject letBullet in pool)
        {
            if (!letBullet.activeSelf)
            {
                letBullet.SetActive(true);
                return letBullet;
            }
        }
        AddToPool();
        return pool[pool.Count - 1];
    }
}
