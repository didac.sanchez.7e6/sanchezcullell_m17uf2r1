using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.PlayerSettings;


public class Sniper : Weapon
{
    public GameObject prefap;
    public List<GameObject> pool;
    public GameObject bullet = null;
    public GameObject rotatinRefernse;
    public static Sniper instance;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
        damage = 5;
    }
    void Start()
    {
        GameObject poolBullet;
        for (int i = 0; i < 20; i++)
        {
            poolBullet = Instantiate(prefap);
            pool.Add(poolBullet);
            poolBullet.SetActive(false);
        }
    }

    public override void Attack()
    {
        int i = 0;
        if (!pool[i].activeInHierarchy)
        {
            bullet = pool[0];
        }
        if (bullet != null)
        {
            bullet.transform.position = transform.position;;
            bullet.transform.rotation = rotatinRefernse.transform.rotation;
            bullet.SetActive(true);
            bullet = null;
        }
    }
    public override void NoAttack()
    {
 
    }
}
